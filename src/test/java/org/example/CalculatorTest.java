package org.example;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @org.junit.jupiter.api.Test
    void sum() {
        assertEquals(4, Calculator.sum(2, 2));
    }

    @org.junit.jupiter.api.Test
    void subtract() {
        assertEquals(0, Calculator.subtract(2, 2));
    }

    @org.junit.jupiter.api.Test
    void divide() {
        assertEquals(1, Calculator.divide(2, 2));
    }

    @org.junit.jupiter.api.Test
    void multiply() {
        assertEquals(4, Calculator.multiply(2, 2));
    }

    @org.junit.jupiter.api.Test
    void multiply2() {
        assertEquals(8, Calculator.multiply(2, 4));
    }
}